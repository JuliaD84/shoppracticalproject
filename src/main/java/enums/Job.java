package enums;

public enum Job {

    CASHIER,
    MANAGER,
    MAINTENANCE_STAFF,
    BRANCH_MANAGER
}
