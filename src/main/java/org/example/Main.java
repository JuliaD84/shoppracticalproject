package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entities.Branch;
import org.example.entities.Suppliers;
import org.example.repository.BranchRepository;
import org.example.repository.SupplierRepository;
import org.hibernate.SessionFactory;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.out.println("Trying to open SessionFactory");
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        System.out.println("Session created");

        BranchRepository branchRepository = new BranchRepository(sessionFactory);
        Branch branch1 = new Branch(null, "Mega Image", "Titulescu");
        branchRepository.create(branch1);

        Branch branch2 = new Branch(null, "Auchan", "Banu Manta");
        branchRepository.create(branch2);
        Branch branch3 = new Branch(null, "La Fourmi", "Camil Ressu");
        branchRepository.create(branch3);

        Branch readBranch = branchRepository.readById(2);
        System.out.println(readBranch);

        List<Branch> readAllBranches = branchRepository.readAll();
        System.out.println(readAllBranches);

        branch2.setName("Auchan Self Service");
        branchRepository.updateDetails(branch2);

        branchRepository.delete(branch1);
        System.out.println(branchRepository.readAll());

        SupplierRepository supplierRepository = new SupplierRepository(sessionFactory);

        Suppliers suppliers1 = new Suppliers(null, "Dorna", "Romania");
        supplierRepository.create(suppliers1);

        Suppliers suppliers2 = new Suppliers(null, "Merlin's Lemonade", "Piatra - Neamt");
        supplierRepository.create(suppliers2);

        Suppliers suppliers3 = new Suppliers(null, "Lavazza", "Italia");
        supplierRepository.create(suppliers3);

        Suppliers readSupplier = (Suppliers) supplierRepository.readById(2);
        System.out.println(readSupplier);

        suppliers1.setName("Dorna vatra Dornei");
        supplierRepository.updateDetails(suppliers1);

        List<Suppliers> suppliersList = supplierRepository.readAll();
        System.out.println(suppliersList);

        supplierRepository.delete(suppliers3);
        System.out.println(supplierRepository.readAll());



    }
}