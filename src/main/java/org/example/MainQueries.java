package org.example;

import enums.Job;
import org.example.config.HibernateConfiguration;
import org.example.entities.Branch;
import org.example.entities.Employee;
import org.example.repository.BranchRepository;
import org.example.repository.EmployeeRepository;
import org.hibernate.SessionFactory;

import static enums.Job.MANAGER;

public class MainQueries {
  static BranchRepository  branchRepository=new BranchRepository(HibernateConfiguration.getSessionFactory());
  static EmployeeRepository employeeRepository=new EmployeeRepository(HibernateConfiguration.getSessionFactory());
    public static void main(String[] args) {

        populateDB();
        System.out.println(branchRepository.getAllBranchesByLocation("Vitan"));
        System.out.println(branchRepository.getAllBranchesByName("Auchan"));
        System.out.println(branchRepository.findByWordInName("Pantelimon"));
        System.out.println(employeeRepository.findByWordInJob(MANAGER));

//afisati toate branch-urile care contin in denumire cuvantul Auchan
//afisati toti angajatii cu functie de manager,


    }
    public static void populateDB(){
        Branch branch1=new Branch(null,"Auchan Banu Manta", "Banu Manta");
        branchRepository.create(branch1);
        Branch branch2=new Branch(null, "Auchan Vitan", "Vitan");
        branchRepository.create(branch2);
        Branch branch3=new Branch(null, "Auchan Pantelimon", "Pantelimon");
        branchRepository.create(branch3);

        Employee employee1 = new Employee(null, "Buda Cristian", Job.CASHIER, branch1);
        Employee employee2 = new Employee(null, "Marius Dumitrache", Job.MANAGER, branch2);
        Employee employee3 = new Employee(null, "Marian Frizeru", Job.BRANCH_MANAGER, branch2);
        Employee employee4 = new Employee(null, "Ombladon Cheloo", Job.CASHIER, branch3);
        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);
        employeeRepository.create(employee4);

    }
}
