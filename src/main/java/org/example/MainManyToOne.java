package org.example;

import enums.Job;
import org.example.config.HibernateConfiguration;
import org.example.entities.Branch;
import org.example.entities.Employee;
import org.example.entities.Product;
import org.example.entities.Suppliers;
import org.example.repository.BranchRepository;
import org.example.repository.EmployeeRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SupplierRepository;
import org.hibernate.SessionFactory;

public class MainManyToOne {
    public static void main(String[] args) {

        BranchRepository branchRepository=new BranchRepository(HibernateConfiguration.getSessionFactory());
        EmployeeRepository employeeRepository=new EmployeeRepository(HibernateConfiguration.getSessionFactory());
        ProductRepository productRepository=new ProductRepository(HibernateConfiguration.getSessionFactory());
        SupplierRepository supplierRepository=new SupplierRepository(HibernateConfiguration.getSessionFactory());


        Branch branch1=new Branch(null,"Kaufland Basarab", "Basarab");
        Branch branch2=new Branch(null, "Kaufland Vitan", "Vitan");
        Branch branch3=new Branch(null,"Kaufland Pipera","Fabrica de Glucoza");
        branchRepository.create(branch1);
        branchRepository.create(branch2);
        branchRepository.create(branch3);
        Employee employee1= new Employee(null,"Buda Cristian", Job.CASHIER, branch1);
        Employee employee2= new Employee(null,"Marius Dumitrache",Job.MANAGER,branch2);
        Employee employee3= new Employee(null,"Ioan Chelaru",Job.MAINTENANCE_STAFF,branch3);
        Employee employee4= new Employee(null,"John Smith",Job.BRANCH_MANAGER,branch1);
        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);
        employeeRepository.create(employee4);
        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println(employee3);
        System.out.println(employee4);
        Suppliers suppliers1=new Suppliers(null, "Dobrogea", "Constanta");
        Suppliers suppliers2=new Suppliers(null, "AlbaLact", "Alba");
        Suppliers suppliers3=new Suppliers(null,"Merlin's Leomnade", "Piatra Neamt");
        supplierRepository.create(suppliers1);
        supplierRepository.create(suppliers2);
        supplierRepository.create(suppliers3);
        Product product1=new Product(null, "Paine de secara","Dobrogea",4.75, 1.37,100,suppliers1, null);
        Product product2=new Product(null, "lapte","Albalact",7.85, 3.99,50,suppliers2,null);
        Product product3=new Product(null, "Limonada","Merlin's Lemonade",8.35, 1.05,200,suppliers3,null);
        productRepository.create(product1);
        productRepository.create(product2);
        productRepository.create(product3);
        System.out.println(product1);
        System.out.println(product2);
        System.out.println(product3);




        //afisati toti employees cu nume, branch, nume jb branch
        //similar cu ce am facut pentru employees-branch, trebuie facut pentru product -supplier


    }
}
