package org.example.repository;

import org.example.entities.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class ProductRepository extends GenericAbstractRepository{
    public ProductRepository(SessionFactory sessionFactory){
        super(sessionFactory);
    }
    public String getEntityName(){
        return "Product";
    }
    public Class<Product> getEntityClass(){
        return Product.class;
    }
    public List<Product> readAllWithCategories(){
        Session session =sessionFactory.openSession();
        List<Product> products=
                session.createQuery("select p from Product p left join fetch p.category", Product.class).getResultList();
        session.close();
        return products;
    }

    public List<Product> getAllProductsByPrice(Double maximumPrice){
        Session session=sessionFactory.openSession();
        List<Product> products=
                session.createQuery("select p from Product p left join fetch p.category where p.price <=" +maximumPrice,Product.class).getResultList();
        session.close();
        return products;
    }
}
