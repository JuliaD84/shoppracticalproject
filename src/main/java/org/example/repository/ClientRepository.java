package org.example.repository;

import org.example.entities.Client;
import org.hibernate.SessionFactory;

public class ClientRepository extends GenericAbstractRepository{
    public ClientRepository(SessionFactory sessionFactory){
        super(sessionFactory);
    }
    public String getEntityName(){
        return "Client";
    }
    public Class<Client> getEntityClass(){
        return Client.class;
    }
}
