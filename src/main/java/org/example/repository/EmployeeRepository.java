package org.example.repository;

import enums.Job;
import org.example.entities.Branch;
import org.example.entities.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class EmployeeRepository extends GenericAbstractRepository{
    public EmployeeRepository(SessionFactory sessionFactory){
        super(sessionFactory);
    }
    public String getEntityName(){
        return "Employee";
    }
    public Class<Employee> getEntityClass(){
        return Employee.class;
    }

    public List<Employee> findByWordInJob(Job word){
        Session session=sessionFactory.openSession();
        String hql="select e from Employee e where job like :wordParam";
        List<Employee> managers=session.createQuery(hql,Employee.class)
                .setParameter("wordParam", word )
                .getResultList();
        session.close();
        return managers;
    }
}
