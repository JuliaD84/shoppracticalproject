package org.example.repository;

import org.example.entities.Branch;
import org.example.entities.Suppliers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Supplier;

public class SupplierRepository extends GenericAbstractRepository{

    public SupplierRepository(SessionFactory sessionFactory){
     super(sessionFactory);

    }

    @Override
    public String getEntityName() {
        return "Suppliers";
    }

    @Override
    public Class<Suppliers> getEntityClass() {
        return Suppliers.class;
    }




}
