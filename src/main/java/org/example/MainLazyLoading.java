package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entities.Category;
import org.example.entities.Product;
import org.example.entities.Suppliers;
import org.example.entities.Suppliers;
import org.example.repository.CategoryRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SupplierRepository;

import java.util.List;

public class MainLazyLoading {

    private static SupplierRepository supplierRepository = new SupplierRepository(HibernateConfiguration.getSessionFactory());
    private static ProductRepository productRepository = new ProductRepository(HibernateConfiguration.getSessionFactory());
    private static CategoryRepository categoryRepository = new CategoryRepository(HibernateConfiguration.getSessionFactory());


    public static void main(String[] args) {

        populateDb();
    }



    private static void populateDb() {

        Suppliers s1 = new Suppliers(null, "Dorna", "Vatra Dornei");
        Suppliers s2 = new Suppliers(null, "Acvila", "Luduș");
        supplierRepository.create(s1);
        supplierRepository.create(s2);

        Category alcohol = new Category(null, "alcohol", Boolean.FALSE);
        Category beverages = new Category(null, "beverages", Boolean.TRUE);
        categoryRepository.create(alcohol);
        categoryRepository.create(beverages);

        Product p1 = new Product(null, "Apa minerala", "Dorna", 2.0, 3.0, 500, s1,null);
        p1.setCategory(List.of(beverages));
        Product p2 = new Product(null, "Apa plata", "Dorna", 2.5, 3.0, 1000, s1, null);
        p2.setCategory(List.of(beverages));
        Product p3 = new Product(null, "Bere", "Tuborg", 2.8, 4.0, 100, s2,null);
        p3.setCategory(List.of(alcohol, beverages));
        productRepository.create(p1);
        productRepository.create(p2);
        productRepository.create(p3);


        System.out.println(productRepository.readAllWithPrintInsideSession());
    }
}





















