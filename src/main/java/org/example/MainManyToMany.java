package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entities.Category;
import org.example.entities.Product;
import org.example.entities.Suppliers;
import org.example.repository.CategoryRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SupplierRepository;

import java.util.List;

public class MainManyToMany {
    public static void main(String[] args) {
        CategoryRepository categoryRepository=new CategoryRepository(HibernateConfiguration.getSessionFactory());
        ProductRepository productRepository=new ProductRepository(HibernateConfiguration.getSessionFactory());
        SupplierRepository supplierRepository=new SupplierRepository(HibernateConfiguration.getSessionFactory());
        Suppliers suppliers1=new Suppliers(null, "Dobrogea", "Constanta");
        Suppliers suppliers2=new Suppliers(null, "AlbaLact", "Alba");
        Suppliers suppliers3=new Suppliers(null,"Merlin's Leomnade", "Piatra Neamt");
        supplierRepository.create(suppliers1);
        supplierRepository.create(suppliers2);
        supplierRepository.create(suppliers3);

        Category category1=new Category(null, "BIO",false);
        Category category2=new Category(null, "ECO",false);
        Category category3=new Category(null, "Alcohol Drinks",true);

        categoryRepository.create(category1);
        categoryRepository.create(category2);
        categoryRepository.create(category3);

        Product product1=new Product(null, "Lapte BIO", "Napolact", 11.02, 5.65,100,suppliers2, List.of(category1,category2));
        Product product2=new Product(null, "Oua", "Tonelli", 16.15, 6.03,60,suppliers2, List.of(category2,category3));
        Product product3=new Product(null, "Avocado", "Avocadoo", 13.07, 4.25,70,suppliers1, List.of(category1,category2));
        Product product4=new Product(null, "Johnny Walker", "Johnny Spirts", 90.82, 42.65,85,suppliers3,List.of(category3) );
         productRepository.create(product1);
         productRepository.create(product2);
         productRepository.create(product3);
        System.out.println( product1);
        System.out.println(product2);
        System.out.println(product3);
        System.out.println(productRepository.readAllWithCategories());
        System.out.println(productRepository.getAllProductsByPrice(20.05));







    }
}
